<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <link rel="stylesheet" href="">
</head>
<body>
    <form action="{{ route('newsletter.create') }}" method="post">
        {{ csrf_field() }}
        <label for="email">Email</label>
        <input type="text" name="email" id="email">

        @if ($errors->has('email'))
            <div class="error">
                {{ $errors->first('email') }}
            </div>
        @endif
        <input type="submit" value="Signup">
    </form>
</body>
</html>